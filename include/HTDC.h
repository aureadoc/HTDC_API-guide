////////////////////////////////////////////////////////////////////////////// 
/// 
/// \mainpage			HTDC Library for ChronoXea device
///
///--------------------------------------------------------------------------- 
/// \section det_sec Details
///
/// - FileName		:	HTDC.h
/// - Dependencies	:   None
/// - Compiler		:   C++
/// - Company		:   Copyright (C) Aurea Technology
/// - Author		:	Matthieu Grovel
///
///	\section des_sec Description
///
///	This header document provide the prototypes and descriptions of all functions    
///	integrated on the dynamic libraries (.dll) files.
/// Those functions allows to control ChronoXea device also named "HTDC"
/// 
///	\section imp_sec Important
///
/// More or less functions are available according to the device type.	
///	The compatibility depends on the device part number recovered by
///	the "HTDC_getSystemVersion" function.								
///	Please see notes functions to check the compatibility with your device:  	
///   -> Device compatibility: PN_HTDC_Mx_x		
///
/// \section rev_sec Revisions
///
/// v1.0 (29/04/19)
/// - First release
///
/// v1.1 (14/11/19)	
/// - Add functions:
///		- TDC_setResultFormat
///		- TDC_getResultFormat
///		- TDC_setCrossCorrelationMode
///		- TDC_getCrossCorrelationData
///		- TDC_getCh1Histogram
///		- TDC_getCh2Histogram
///		- TDC_getCh3Histogram
///		- TDC_getCh4Histogram
///
/// v1.2 (24/04/20)	
///	- Rename functions:
///		- listDevices to TDC_listDevices
///		- openDevice to TDC_openDevice
///		- closeDevice to TDC_closeDevice
///	- Modify functions:
///		- TDC_setSyncInputConfig
///		- TDC_getSyncInputConfig
///		- TDC_setChannelConfig
///		- TDC_getChannelConfig
///	- Add functions:
///		- TDC_setSyncSource
///		- TDC_getSyncSource
///		- TDC_setSyncDivider
///		- TDC_getSyncDivider
///		- TDC_setChannelDelay
///		- TDC_getChannelDelay
///
/// v1.3 (01/06/20)
///	- Modify functions:
///		- TDC_armChannel: add time condition
///	- Add functions:
///		- TDC_setDataRate
///		- TDC_getDataRate
///		- TDC_setMeasMode
///		- TDC_getMeasMode
///		- TDC_getCh1OneShotMeasurement
///		- TDC_getCh2OneShotMeasurement
///		- TDC_getCh3OneShotMeasurement
///		- TDC_getCh4OneShotMeasurement
///
/// v1.4 (13/10/20)
/// - Add function:
///		- TDC_getSystemVersion
///		- TDC_getSystemFeature
/// 
/// v1.5 (26/02/21)
///	- Improve function:
///		- TDC_getChxData: return correct n data according to consign
///	- Add comments on functions header
///
/// v1.6 (15/03/21)
///	- Improve functions
///		- TDC_armCh: allows inter-correlation using 
///	- Add comments instruction for 'TDC_getCrossCorrelationData' function
///				
/// v1.7 (10/05/21)
/// - Improve internal data process
/// - Modify comments for 'TDC_getCrossCorrelationData' function
/// 
/// v1.8 (24/07/21)
/// - Fix erroneous time tag data 
///
/// v1.9 (14/12/21)
/// - Improve range to 7s
/// 
/// v1.10 (15/01/22)
/// - Improve threads
/// 
/// v1.11 (14/04/22)
/// - Add internal mutex
///
/// v2.0 (14/04/22)
/// - Add Device index in all functions (for multi device control)
/// - Modify all get functions to return value by pointer
/// - Add Wrapper
/// - Handle multiple device application
/// - Replace ``TDC_`` by ``HTDC_`` at the beginning of all functions
/// 
/// v2.01 (24/11/22)
/// - Change libusb interface index to open multiple devices	
/// 
/// v2.02 (30/08/23)
/// - Add Python functions
///
/// v2.03 (21/12/23)
/// - Cap divider to 511, according to IC maximum value	
///
/// v2.04 (27/02/24)
/// - Cap sync internal clock to 25 MHz for tag only usage.
/// 
/// v2.05 (29/10/24)
/// - Add Save1 for User Parameter
/// - Add Factory Parameter Settings Command
/// 
////////////////////////////////////////////////////////////////////////////// 

#ifndef _HTDC_H
#define _HTDC_H

#ifndef DOXYGEN_SHOULD_SKIP_THIS

/// @brief Correlator resolution
#define HTDC_RES		0.013	///< Time to digital resolution in ns

/// Channels mask
/// Used as parameters to select target channels
#define CH_ALL		0xF		///< all channels mask
#define CH_1		(1<<0)	///< channel 1 mask
#define	CH_2		(1<<1)	///< channel 2 mask
#define CH_3		(1<<2)	///< channel 3 mask
#define CH_4		(1<<3)	///< channel 4 mask
#define CH_CROSS	(1<<4)	///< channel x cross mask

// Define LIB_CALL for any platform
#if defined _WIN32 || defined __CYGWIN__
#ifdef WIN_EXPORT
#ifdef __GNUC__
#define LIB_CALL __attribute__ ((dllexport))
#else
#define LIB_CALL __declspec(dllexport) __cdecl
#endif
#else
#ifdef __GNUC__
#define LIB_CALL __attribute__ ((dllimport))
#else
#define LIB_CALL __declspec(dllimport) 
#endif
#endif
#else
#if __GNUC__ >= 4
#define LIB_CALL __attribute__ ((visibility ("default")))
#else
#define LIB_CALL
#endif
#endif

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <Windows.h>
#include "conio.h"
#define delay(x) Sleep(x)
#elif __unix
#include <cstring>
#include <unistd.h>
#define delay(x) usleep(x*1000)
#else
#include <unistd.h>
#define delay(x) usleep(x*1000)
#endif

#ifdef _WIN32 
#define secure_strtok strtok_s
#else
#define secure_strtok strtok_r
#endif	

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

#if defined(__cplusplus)
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short HTDC_getLibVersion(unsigned short *value)
///	\brief		Get the librarie version
/// \details	Return the version librarie in format 0x0000MMmm \n
///				with: MM=major version \n
///					  mm=minor version			  
///
/// \param		*value	return lib version by pointer \n
///						Format: 0xMMmm		\n
///						with:	MM: major version	\n
///								mm: minor version
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
short LIB_CALL HTDC_getLibVersion(unsigned short* value);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short HTDC_listDevices(char** devices, short* number)
/// \brief		List all the devices connected
/// \details	List all ChronoXea devices connected to the computer 
///	\note		Mandatory to do before any other actions on the device. \n
///	\note		Device compatibility: all
///
/// \param		*devices pointer to the table buffer which contain list of devices connected \n
///						 Output format: "deviceName - serialNumber" \n
///						 Example:									\n
///							devices[0]="ChronoXea - SN_12345678910" \n
///							devices[1]="ChronoXea - SN_10987654321" \n
///
/// \param		*number pointer to the number of devices connected
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    
///		
short LIB_CALL HTDC_listDevices(char** devices, short* number);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short HTDC_openDevice(short iDev)
/// \brief		Open and initialize target device
/// \details	Open and initialize the target ChronoXea device
///	\note		Mandatory to used after "HTDC_listDevices" and before any other actions on the device. \n
///	\note		Device compatibility: all
///
/// \param		iDev	Device index indicate by "HTDC_listDevices" function \n
///						Between 0 to n (indicated from "HTDC_listDevices" function)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
short LIB_CALL HTDC_openDevice(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short HTDC_closeDevice(short iDev)
///	\brief		Close device
/// \details	Close ChronoXea device previously opened. 
///	\note		Mandatory to do at the end of system control. \n
///	\note		Device compatibility: all
///
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_closeDevice(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getSystemVersion(short iDev, char *version) 
/// \brief		Get system version
/// \details	Get system version: Serial number, product number and firmware version
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" functions
///
/// \param		version	Pointer to the buffer which receive the system version. \n
///						String format: "SN_xxxxxxxxxxx:PN_HTDC_Mx_xx:FN_x.xx"    \n
///						The receive buffer size must be of 64 octets min.
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_getSystemVersion(short iDev, char* version);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getSystemFeature(short iDev, short iFeature, short* value)
///	\brief		Get system feature
/// \details	Read system memory to recover one system feature
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iFeature	index feature to get \n
///							0: channel number. Return: 0 (1 channel) to 3 (4 channels)\n
///							1: integration mode. Return: 0 (standalone) or 1 (OEM)
/// 
/// \param		*value		value of the target feature
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_getSystemFeature(short iDev, short iFeature, short* value);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_setSyncSource(short iDev, unsigned short mode)
/// \brief		Configure the Sync signal source
/// \details	Configure the HTDC sync clock source (start of HTDC) between internal or external  
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		mode	source mode: 0=external, 1=internal 
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_setSyncSource(short iDev, unsigned short mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getSyncSource(short iDev, unsigned short *mode)
/// \brief		Configure the Sync signal source
/// \details	Configure the HTDC sync clock source (start of HTDC) between internal or external  
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" functions
///
/// \param		*mode	return by pointer the source mode: 0=external, 1=internal 
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_getSyncSource(short iDev, unsigned short* mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_setInternalSyncFrequency(short iDev, unsigned int value)
/// \brief		Set the internal sync frequency 
/// \details	Adjust frequency of sync signal generated in internal from a synthesizer.
/// \note		This internal frequency is only applyed on HTDC sync input if sync source 
///				set in internal mode.
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		value frequency value adjusting between 1Hz to 4MHz (by step of 4ns); 25MHz max in TagOnly mode.
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_setInternalSyncFrequency(short iDev, unsigned int value);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getInternalSyncFrequency(short iDev, unsigned int *value)
/// \brief		Get the internal sync frequency 
/// \details	Recover the current frequency of sync signal generated in internal from a synthesizer.
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		*value	return by pointer the frequency value. Between 1Hz to 4MHz (by step of 4ns)
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_getInternalSyncFrequency(short iDev, unsigned int* value);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_setSyncDivider(short iDev, unsigned short value)
/// \brief		Set the sync divider 
/// \details	Adjust the divider applyed on the sync signal. As HTDC start input do  
///				not exceed 4MHz, clock divider function is available to decrease 
///				the sync frequency. 
/// \note		The divider is active for both the external and internal sync signal. \n
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		value	divider value between 1 to 511 by step of 1.
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_setSyncDivider(short iDev, unsigned short value);

/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getSyncDivider(short iDev, unsigned short *value)
/// \brief		Get the sync divider 
/// \details	Recover the current divider apply on the sync signal. As HTDC start input  
///				do not exceed 4MHz, clock divider function is available to decrease 
///				the sync frequency. 
/// \note		The divider is active for both the external and internal sync signal.
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		*value	return by pointer the divider value. Between 1 to 511 by step of 1.
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_getSyncDivider(short iDev, unsigned short* value);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_setSyncInputConfig(short iDev, unsigned short enable, unsigned short edge, unsigned short level)
/// \brief		Configure Sync input
/// \details	Configure state (ON or OFF), trigger edge and level (TTL-CMOS or NIM) of sync input 
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		enable		enable sync input: 0=OFF, 1=ON 
/// 
/// \param		edge		edge of threshold signal: 0=rising, 1=falling
/// 
/// \param		level		level of input signal: 0=TTL-CMOS, 1=NIM
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_setSyncInputConfig(short iDev, unsigned short enable, unsigned short edge, unsigned short level);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getSyncInputConfig(short iDev, unsigned short *enable, unsigned short *edge, unsigned short *level)
/// \brief		Get Sync input configuration
/// \details	Get state (ON or OFF), trigger edge and level (TTL-CMOS or NIM) of sync input
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		*enable		return by pointer the sync input state. Return values: 0=OFF, 1=ON 
/// 
/// \param		*edge		return by pointer the edge of threshold signal. Return values 0=rising, 1=falling 
/// 
/// \param		*level		return by pointer the level of input. Return values: 0=TTL-CMOS, 1=NIM
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_getSyncInputConfig(short iDev, unsigned short* enable, unsigned short* edge, unsigned short* level);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_setChannelDelay(short iDev, unsigned char iCh, float delay)
/// \brief		Set channel(s) delay
/// \details	Allows to control delay on specific(s) channel(s).
///				This is a additional delay between 0 to 10.0ns by step of 10ps relative to the Sync input.
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iCh		index of channel(s) to set: CH_1, CH_2, CH3, CH4 (depends of channels number available)   \n
///						Parameter ORING with others channels id. (ex: HTDC_setChannelDelay(CH_1|CH_2);)
/// 
///	\param		delay	delay in ns (between 0.0 to 10.0ns by step of 0.01ns)
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///				
short LIB_CALL HTDC_setChannelDelay(short iDev, unsigned char iCh, float delay);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getChannelDelay(short iDev, unsigned char iCh, float *delay)
/// \brief		Get channel delay
/// \details	Allows to get the current delay applyed on the target channel.
///	\note		Device compatibility: all
///
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
/// 
/// \param		iCh		index of channel(s) to set: CH_1, CH_2, CH3, CH4 (depends of channels number available)
/// 
///	\param		*delay	return by pointer the delay in ns (between 0.00 to 10.00ns by step of 0.01ns)
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///				
short LIB_CALL HTDC_getChannelDelay(short iDev, unsigned char iCh, float* delay);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_setChannelConfig(short iDev, unsigned char iCh, unsigned short power, unsigned short edge, unsigned short level)
/// \brief		Set channel(s) configuration
/// \details	Allows to configurate specific(s) channel(s) to activate power, threshold edge and input level
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iCh		index of channel(s) to set: CH_1, CH_2, CH3, CH4 (depends of channels number available)   \n
///						Parameter ORING with others channels id. (ex: HTDC_setChannelConfig(CH_1|CH_2);)
/// 
///	\param		power	channel power supply: 0=OFF or 1=ON
/// 
///	\param		edge	input signal edge sensibility: 0=rising or 1=falling
/// 
/// \param		level	level of input signal: 0=TTL-CMOS, 1=NIM
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///				
short LIB_CALL HTDC_setChannelConfig(short iDev, unsigned char iCh, unsigned short power, unsigned short edge, unsigned short level);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getChannelConfig(short iDev, unsigned char iCh, unsigned short *power, unsigned short *edge, unsigned short *level)
/// \brief		Get channel configuration
/// \details	Allows to get the configuration of the target channel as the power state, edge and level input.
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iCh		index of channel to set: CH_1, CH_2, CH3, CH4 (depends of channels number available)
/// 
///	\param		*power	return by pointer the actual state of the channel power supply: 0=OFF or 1=ON
/// 
///	\param		*edge	return by pointer the input signal edge sensibility: 0=rising or 1=falling
/// 
/// \param		*level	return by pointer the level of input. Return values: 0=TTL-CMOS, 1=NIM
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///						
short LIB_CALL HTDC_getChannelConfig(short iDev, unsigned char iCh, unsigned short* power, unsigned short* edge, unsigned short* level);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_armChannel(short iDev, unsigned char iCh, long nSample, long time)
/// \brief		Arm target channel(s)
/// \details	Arm target channel(s) to recover data either on a sample number or on a fix time.
///	\note		For a correct behavior, be careful to set ONLY ONE arming condition (the other
///				parameter must be set to -1 (infinite))
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iCh			index of channel to set: CH_1, CH_2, CH3, CH4 (depends of channels number available)  \n
///							Parameter ORING with others channels id. (ex: HTDC_armChannel(CH_1|CH_2);)
///	\param		nSample		number of sample time to recover. \n
///							Values to set:  \n
///							 -1=no condition (infinite) \n
///							 x=between 1 to 2^31 by step of 1 
/// \param		time		time of the acquisition in ms. \n
///							Values to set: \n 
///							 -1=no time condition (infinite) \n
///							 x=between 100 to 2^31 by step of 100ms
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///						
short LIB_CALL HTDC_armChannel(short iDev, unsigned char iCh, long nSample, long time);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getChannelState(short iDev, unsigned char iCh, int *status, unsigned long *nSampleToRecover, unsigned long *nSampleRecovered)
/// \brief		Get channel state 
/// \details	Return the state of the target channel: status, number of sample to recover and consign
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iCh					index of channel to set: CH_1, CH_2, CH3, CH4 (depends of channels number available)
/// 
///	\param		*status				pointer to status of channel: 0=stopped, 1=armed or 2=running
/// 
///	\param		*nSampleToRecover	pointer to the number of sample to recover
/// 
///	\param		*nSampleRecovered	pointer to the number of sample recovered
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///						
short LIB_CALL HTDC_getChannelState(short iDev, unsigned char iCh, int* status, unsigned long* nSampleToRecover, unsigned long* nSampleRecovered);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_startChannel(short iDev, unsigned char iCh)
/// \brief		Start target channel(s) 
/// \details	Start the target channel(s) if it was previously armed.
/// \note		Be careful to arm the target channel(s) before to use this function
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iCh		index of channel(s): CH_1, CH_2, CH3, CH4 (depends of channels number available) \n
///						Parameter ORING with others channels id. (ex: HTDC_startChannel(CH_1|CH_2);)
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///						
short LIB_CALL HTDC_startChannel(short iDev, unsigned char iCh);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_stopChannel(short iDev, unsigned char iCh)
/// \brief		Stop target channel(s) 
/// \details	Stop the target channel(s). 
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iCh		index of channel(s): CH_1, CH_2, CH3, CH4 (depends of channels number available)  \n
///						Parameter ORING with others channels id. (ex: HTDC_stopChannel(CH_1|CH_2);)
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///						
short LIB_CALL HTDC_stopChannel(short iDev, unsigned char iCh);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh1Data(short iDev, unsigned long long* data, unsigned long* count)
/// \brief		Get channel 1 data 
/// \details	Get channel 1 data in several format according to the mode set by the
///				"HTDC_setResultFormat" function.  \n
///				Thus the user can get only HTDC's raw data, time tagging or both. \n
///	\note		This function should be polled until the conditions, previously set 
///				from "HTDC_armChannel" function, are reached.
///				The maximum pooling rate of this function depends of the data rate 
///				value set by "HTDC_setDataRate" function.
///	\note		Device compatibility: all
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///				
/// \param		*data	pointer to the target buffer (in 64bits) to store data.  \n
///						Note: target buffer must be sized according to the number of data to recover. \n
///						According to the result format mode set by HTDC_setResultFormat function 
///						the user can recover data with different formats:
///						* in HTDC raw data (mode 0): \n
///							Only HTDC's raw data are provided and given in multiple of HTDC_RES (0.013ns). \n
///							Format in buffer: \n
///								data[0]=0x000000RRRRRRRRRR \n
///								data[1]=0x000000RRRRRRRRRR \n
///								... \n
///							with: \n
///								RRRRRRRRRR: 5 bytes of HTDC's raw data.
///											Represent the time value in multiple of 0.013ns: time=data[x]*0.013
///						* in HTDC raw data + time tagging (mode 1): \n
///							Both tds's raw data and time tagging. \n 
///							Format in buffer: \n
///								data[0]=0xTTTTTTTTRRRRRRRR \n
///								data[1]=0xTTTTTTTTRRRRRRRR \n
///								... \n
///							with: \n
///								TTTTTTTT: 4 bytes (MSB) of time tagging value. 
///										  Represent the number of Sync period until a ch1 event. \n
///								RRRRRRRR: 4 bytes (LSB) of HTDC's raw data.
///										  Represent the time value between the last Sync and the ch1 event.
///										  In multiple of 0.013ns: time=data[x]*0.013  \n
///						* in time tagging mode (mode 3): \n
///							Only time tagging value is returned. \n
///							Format in buffer: \n
///								data[0]=0x000000TTTTTTTTTT \n
///								data[1]=0x000000TTTTTTTTTT \n
///								... \n
///							with: \n
///								TTTTTTTTTT: 5 bytes of time tigging value.
///											Represent the number of Sync period until a ch1 event		
/// 
/// \param		*count		return by pointer the number of data recovered.		
///							
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh1Data(short iDev, unsigned long long* data, unsigned long* count);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh1Histogram(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double *binWidth, long *count)
/// \brief		Get channel 1 histogram 
/// \details	Get the histogram of the time data measured from channel 1. \n
///				User can set the range of the histogram to recover or let the function \n
///				adjust automatically the best range. As the histogram is represented \n
///				on two axes of 65536 values, the bin width is automatically adjusted to \n
///				respect this deep. \n
///	\note		This function should be polled until the conditions, previously set 
///				from "HTDC_armChannel" function, are reached.
///				The maximum pooling rate of this function depends of the data rate 
///				value set by "HTDC_setDataRate" function.
///	\note		Device compatibility: all
///				 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///				
/// \param		Xmin		min value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmin=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the min value according \n
///							to the input signal measured.
///
/// \param		Xmax		max value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmax=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the max value according  \n
///							to the input signal measured.
///
/// \param		*histoX		pointer to target buffer for X axis histogram values recovery. \n
///							Each row represent the time value of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*histoY		pointer to target buffer for Y axis histogram values recovery. \n
///							Each row represent the number of occurences of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*binWidth	return by pointer the value of binWidth in ns. \n
///							If the range of the measured signal is upper 65536x13ps (851ns) \n
///							the function automatically adjust the bin width to respect the  \n
///							Y axis deep of maximum 65536 values.		
/// 
/// \param		*count		return by pointer the number of data recovered.	
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh1Histogram(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh2Data(short iDev, unsigned long long* data, unsigned long* count)
/// \brief		Get channel 2 data 
/// \details	Get channel 2 data in several format according to the mode set by the
///				"HTDC_setResultFormat" function.  \n
///				Thus the user can get only HTDC's raw data, time tagging or both. \n
///	\note		This function should be polled until the conditions, previously set 
///				from "HTDC_armChannel" function, are reached.
///				The maximum pooling rate of this function depends of the data rate 
///				value set by "HTDC_setDataRate" function.
///	\note		Device compatibility: all
///				 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///				
/// \param		*data	pointer to the target buffer (in 64bits) to store data.  \n
///						Note: target buffer must be sized according to the number of data to recover. \n
///						According to the result format mode set by HTDC_setResultFormat function 
///						the user can recover data with different formats:
///						* in HTDC raw data (mode 0): \n
///							Only HTDC's raw data are provided and given in multiple of HTDC_RES (0.013ns). \n
///							Format in buffer: \n
///								data[0]=0x000000RRRRRRRRRR \n
///								data[1]=0x000000RRRRRRRRRR \n
///								... \n
///							with: \n
///								RRRRRRRRRR: 5 bytes of HTDC's raw data.
///											Represent the time value in multiple of 0.013ns: time=data[x]*0.013
///						* in HTDC raw data + time tagging (mode 1): \n
///							Both tds's raw data and time tagging. \n 
///							Format in buffer: \n
///								data[0]=0xTTTTTTTTRRRRRRRR \n
///								data[1]=0xTTTTTTTTRRRRRRRR \n
///								... \n
///							with: \n
///								TTTTTTTT: 4 bytes (MSB) of time tagging value. 
///										  Represent the number of Sync period until a ch2 event. \n
///								RRRRRRRR: 4 bytes (LSB) of HTDC's raw data.
///										  Represent the time value between the last Sync and the ch2 event.
///										  In multiple of 0.013ns: time=data[x]*0.013  \n
///						* in time tagging mode (mode 3): \n
///							Only time tagging value is returned. \n
///							Format in buffer: \n
///								data[0]=0x000000TTTTTTTTTT \n
///								data[1]=0x000000TTTTTTTTTT \n
///								... \n
///							with: \n
///								TTTTTTTTTT: 5 bytes of time tigging value.
///											Represent the number of Sync period until a ch2 event		
/// 
/// \param		*count		return by pointer the number of data recovered.		
///							
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh2Data(short iDev, unsigned long long* data, unsigned long* count);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh2Histogram(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double *binWidth, long *count)
/// \brief		Get channel 2 histogram 
/// \details	Get the histogram of the time data measured from channel 2. \n
///				User can set the range of the histogram to recover or let the function \n
///				adjust automatically the best range. As the histogram is represented \n
///				on two axes of 65536 values, the bin width is automatically adjusted to \n
///				respect this deep. \n
///	\note		This function should be polled until the conditions, previously set 
///				from "HTDC_armChannel" function, are reached.
///				The maximum pooling rate of this function depends of the data rate 
///				value set by "HTDC_setDataRate" function.
///	\note		Device compatibility: all
///				
/// \param		iDev	Device index indicate by "HTDC_listDevices" function 
///				
/// \param		Xmin		min value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmin=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the min value according \n
///							to the input signal measured.
///
/// \param		Xmax		max value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmax=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the max value according  \n
///							to the input signal measured.
///
/// \param		*histoX		pointer to target buffer for X axis histogram values recovery. \n
///							Each row represent the time value of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*histoY		pointer to target buffer for Y axis histogram values recovery. \n
///							Each row represent the number of occurences of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*binWidth	return by pointer the value of binWidth in ns. \n
///							If the range of the measured signal is upper 65536x13ps (851ns) \n
///							the function automatically adjust the bin width to respect the  \n
///							Y axis deep of maximum 65536 values.			
/// 
/// \param		*count		return by pointer the number of data recovered.
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh2Histogram(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh3Data(short iDev, unsigned long long* data, unsigned long* count)
/// \brief		Get channel 3 data 
/// \details	Get channel 3 data in several format according to the mode set by the
///				"HTDC_setResultFormat" function.  \n
///				Thus the user can get only HTDC's raw data, time tagging or both. \n
///	\note		This function should be polled until the conditions, previously set 
///				from "HTDC_armChannel" function, are reached.
///				The maximum pooling rate of this function depends of the data rate 
///				value set by "HTDC_setDataRate" function.
///	\note		Device compatibility: all
///				
/// \param		iDev	Device index indicate by "HTDC_listDevices" function 
///				
/// \param		*data	pointer to the target buffer (in 64bits) to store data.  \n
///						Note: target buffer must be sized according to the number of data to recover. \n
///						According to the result format mode set by HTDC_setResultFormat function 
///						the user can recover data with different formats:
///						* in HTDC raw data (mode 0): \n
///							Only HTDC's raw data are provided and given in multiple of HTDC_RES (0.013ns). \n
///							Format in buffer: \n
///								data[0]=0x000000RRRRRRRRRR \n
///								data[1]=0x000000RRRRRRRRRR \n
///								... \n
///							with: \n
///								RRRRRRRRRR: 5 bytes of HTDC's raw data.
///											Represent the time value in multiple of 0.013ns: time=data[x]*0.013
///						* in HTDC raw data + time tagging (mode 1): \n
///							Both tds's raw data and time tagging. \n 
///							Format in buffer: \n
///								data[0]=0xTTTTTTTTRRRRRRRR \n
///								data[1]=0xTTTTTTTTRRRRRRRR \n
///								... \n
///							with: \n
///								TTTTTTTT: 4 bytes (MSB) of time tagging value. 
///										  Represent the number of Sync period until a ch3 event. \n
///								RRRRRRRR: 4 bytes (LSB) of HTDC's raw data.
///										  Represent the time value between the last Sync and the ch3 event.
///										  In multiple of 0.013ns: time=data[x]*0.013  \n
///						* in time tagging mode (mode 3): \n
///							Only time tagging value is returned. \n
///							Format in buffer: \n
///								data[0]=0x000000TTTTTTTTTT \n
///								data[1]=0x000000TTTTTTTTTT \n
///								... \n
///							with: \n
///								TTTTTTTTTT: 5 bytes of time tigging value.
///											Represent the number of Sync period until a ch3 event		
/// 
/// \param		*count		return by pointer the number of data recovered.		
///							
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh3Data(short iDev, unsigned long long* data, unsigned long* count);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh3Histogram(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double *binWidth, long *count)
/// \brief		Get channel 3 histogram 
/// \details	Get the histogram of the time data measured from channel 3. \n
///				User can set the range of the histogram to recover or let the function \n
///				adjust automatically the best range. As the histogram is represented \n
///				on two axes of 65536 values, the bin width is automatically adjusted to \n
///				respect this deep. \n
///	\note		This function should be polled until the conditions, previously set 
///				from "HTDC_armChannel" function, are reached.
///				The maximum pooling rate of this function depends of the data rate 
///				value set by "HTDC_setDataRate" function.
///	\note		Device compatibility: all
///				
/// \param		iDev	Device index indicate by "HTDC_listDevices" function 
///				
/// \param		Xmin		min value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmin=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the min value according \n
///							to the input signal measured.
///
/// \param		Xmax		max value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmax=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the max value according  \n
///							to the input signal measured.
///
/// \param		*histoX		pointer to target buffer for X axis histogram values recovery. \n
///							Each row represent the time value of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*histoY		pointer to target buffer for Y axis histogram values recovery. \n
///							Each row represent the number of occurences of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*binWidth	return by pointer the value of binWidth in ns. \n
///							If the range of the measured signal is upper 65536x13ps (851ns) \n
///							the function automatically adjust the bin width to respect the  \n
///							Y axis deep of maximum 65536 values.			
/// 
/// \param		*count		return by pointer the number of data recovered.
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh3Histogram(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh4Data(short iDev, unsigned long long* data, unsigned long* count)
/// \brief		Get channel 4 data 
/// \details	Get channel 4 data in several format according to the mode set by the
///				"HTDC_setResultFormat" function.  \n
///				Thus the user can get only HTDC's raw data, time tagging or both. \n
///	\note		This function should be polled until the conditions, previously set 
///				from "HTDC_armChannel" function, are reached.
///				The maximum pooling rate of this function depends of the data rate 
///				value set by "HTDC_setDataRate" function.
///	\note		Device compatibility: all
///				
/// \param		iDev	Device index indicate by "HTDC_listDevices" function 
///				
/// \param		*data	pointer to the target buffer (in 64bits) to store data.  \n
///						Note: target buffer must be sized according to the number of data to recover. \n
///						According to the result format mode set by HTDC_setResultFormat function 
///						the user can recover data with different formats:
///						* in HTDC raw data (mode 0): \n
///							Only HTDC's raw data are provided and given in multiple of HTDC_RES (0.013ns). \n
///							Format in buffer: \n
///								data[0]=0x000000RRRRRRRRRR \n
///								data[1]=0x000000RRRRRRRRRR \n
///								... \n
///							with: \n
///								RRRRRRRRRR: 5 bytes of HTDC's raw data.
///											Represent the time value in multiple of 0.013ns: time=data[x]*0.013
///						* in HTDC raw data + time tagging (mode 1): \n
///							Both tds's raw data and time tagging. \n 
///							Format in buffer: \n
///								data[0]=0xTTTTTTTTRRRRRRRR \n
///								data[1]=0xTTTTTTTTRRRRRRRR \n
///								... \n
///							with: \n
///								TTTTTTTT: 4 bytes (MSB) of time tagging value. 
///										  Represent the number of Sync period until a ch4 event. \n
///								RRRRRRRR: 4 bytes (LSB) of HTDC's raw data.
///										  Represent the time value between the last Sync and the ch4 event.
///										  In multiple of 0.013ns: time=data[x]*0.013  \n
///						* in time tagging mode (mode 3): \n
///							Only time tagging value is returned. \n
///							Format in buffer: \n
///								data[0]=0x000000TTTTTTTTTT \n
///								data[1]=0x000000TTTTTTTTTT \n
///								... \n
///							with: \n
///								TTTTTTTTTT: 5 bytes of time tigging value.
///											Represent the number of Sync period until a ch4 event	
/// 
/// \param		*count		return by pointer the number of data recovered.			
///							
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh4Data(short iDev, unsigned long long* data, unsigned long* count);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh4Histogram(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double *binWidth, long *count)
/// \brief		Get channel 4 histogram 
/// \details	Get the histogram of the time data measured from channel 4. \n
///				User can set the range of the histogram to recover or let the function \n
///				adjust automatically the best range. As the histogram is represented \n
///				on two axes of 65536 values, the bin width is automatically adjusted to \n
///				respect this deep. \n
///	\note		This function should be polled until the conditions, previously set 
///				from "HTDC_armChannel" function, are reached.
///				The maximum pooling rate of this function depends of the data rate 
///				value set by "HTDC_setDataRate" function.
///	\note		Device compatibility: all
///			
/// \param		iDev	Device index indicate by "HTDC_listDevices" function	 
///				
/// \param		Xmin		min value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmin=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the min value according \n
///							to the input signal measured.
///
/// \param		Xmax		max value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmax=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the max value according  \n
///							to the input signal measured.
///
/// \param		*histoX		pointer to target buffer for X axis histogram values recovery. \n
///							Each row represent the time value of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*histoY		pointer to target buffer for Y axis histogram values recovery. \n
///							Each row represent the number of occurences of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*binWidth	return by pointer the value of binWidth in ns. \n
///							If the range of the measured signal is upper 65536x13ps (851ns) \n
///							the function automatically adjust the bin width to respect the  \n
///							Y axis deep of maximum 65536 values.		
/// 
/// \param		*count		return by pointer the number of data recovered.	
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh4Histogram(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_setCrossCorrelationALU(short iDev, unsigned short iMeas)
/// \brief		Set cross correlation ALU
/// \details	Set the ALU's measure type to apply the cross correlation. 
/// \note		Be careful to select the correct type to have a positive result.
///	\note		Device compatibility: all
///				
/// \param		iDev	Device index indicate by "HTDC_listDevices" function 
///				
/// \param		iMeas	Measure type available: \n
///						 0: CH1 - CH2 \n
///						 1: CH2 - CH1 \n
///						 2: CH2 - CH3 \n
///						 3: CH3 - CH2
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_setCrossCorrelationALU(short iDev, unsigned short iMeas);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCrossCorrelationData(short iDev, unsigned long long* data, unsigned long* count)
/// \brief		Get channel cross correlation data
/// \details	Get samples time measured from cross correlation. \n
///				Allows to recover only cross correlation data.
///				To use this function, the "result format" of both target channels  
///				MUST BE adjusted in "HTDC raw data + time tagging" mode.
///				Respect the correct initialization and order of the functions: \n
///				  * 1: Set result data format to "HTDC raw data + time tagging"
///				  * 2: Init the 2 target channels		
///				  * 3: Set channels measurement mode in Continuous
///				  * 4: Init ALU to calculate time between 2 channels
///				  * 5: Arm target channels
///				  * 6: Arm x cross channel (which do calcul) 
///				  * 7: Start target channels + x cross
///				  * 8: Get data of the cross correlation
///				  * 9: Stop channels + x cross
///
///				Example of use:		\n
///				 * HTDC_setResultFormat(CH_1|CH_2, 1);			// Set result format to "HTDC raw data + time tagging"
///				 * HTDC_setChannelConfig(CH_1|CH_2, 1, 0, 0);	// Init channel: power ON, rising edge, TTL-CMOS
///				 * HTDC_setMeasMode(CH_1|CH_2, 1);				// Set channel measurement mode to OneShot
///				 * HTDC_setCrossCorrelationALU(1);				// Set ALU to calcul CH2 - CH1 
///				 * HTDC_armChannel(CH_1|CH_2, -1, -1);			// Arm channels to recover in continuous 
///				 * HTDC_armChannel(CH_CROSS, 200, -1);			// Arm x Cross to recover 200 sample 
///				 * HTDC_startChannel(CH_1|CH_2|CH_CROSS);		// Start channels
///				 * HTDC_getCrossCorrelationData(data);			// Get result data
///				 * HTDC_stopChannel(CH_1|CH_2|CH_CROSS);			// Stop channels
///
///	\note		This function should be polled until the conditions, previously set 
///				from "HTDC_armChannel" function on each target channels, are reached.
///				The maximum pooling rate of this function depends of the data rate 
///				value set by "HTDC_setDataRate" function.
///	\note		Device compatibility: all
///				 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///				
/// \param		*data	pointer to the target buffer (in 64bits) to store data. \n
///						Target buffer must be sized according to the number of data to recover. \n
///						Each data are on raw data format and given in multiple of HTDC_RES (0.013ns). \n
///						Format in buffer: \n
///							data[0]=0x000000RRRRRRRRRR \n
///							data[1]=0x000000RRRRRRRRRR \n
///							... \n
///						with: \n
///							RRRRRRRRRR: 5 bytes of HTDC's raw data.
///										Represent the time value in multiple of 0.013ns: time=data[x]*0.013
///
/// \param      *count      return by pointer the number of data recovered. 
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCrossCorrelationData(short iDev, unsigned long long* data, unsigned long* count);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_setResultFormat(short iDev, unsigned char iCh, unsigned short mode)
/// \brief		Set result format 
/// \details	Set target channel(s) raw data result in the prefered format mode. \n
///				User can set the data format recovered from "HTDC_getChXData" functions \n
///				either with HTDC raw data, raw and time tag or only with time tagging. \n
///				Depending on the format the features are different: \n
///				 * "HTDC raw data" : 39bits of data time (~7s of measurement time range)
///				 * "HTDC raw data + time tagging": 32bits of data time (55ms of range) + 32bits of tag data
///				 * "time tagging" : 40bits of tag data
///
/// \note		By default the format is in HTDC raw data.
///	\note		Device compatibility: all
///				
/// \param		iDev	Device index indicate by "HTDC_listDevices" function 
///
/// \param		iCh		index of channel(s): CH_1, CH_2, CH3, CH4 (depends of channels number available)  \n
///						Parameter ORING with others channels id. (ex: HTDC_setResultFormat(CH_1|CH_2);)
///	\param		mode	format result: \n
///							0: HTDC raw data \n
///							1: HTDC raw data + time tagging \n
///							2: time tagging
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///					
short LIB_CALL HTDC_setResultFormat(short iDev, unsigned char iCh, unsigned short mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getResultFormat(short iDev, unsigned char iCh, unsigned short* mode)
/// \brief		Get result format 
/// \details	Get target channel raw data result in the prefered format mode. \n
///				Get the result data format return from "HTDC_getChXData" functions, \n
///				either with HTDC raw data, raw and time tag or only with time tagging.
///	\note		Device compatibility: all
///				 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iCh		index of channel(s): CH_1, CH_2, CH3, CH4 (depends of channels number available)
/// 
///	\param		mode	return by value the format result: \n
///							0: HTDC raw data \n
///							1: HTDC raw data + time tagging \n
///							2: time tagging
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
short LIB_CALL HTDC_getResultFormat(short iDev, unsigned char iCh, unsigned short* mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getEventsCounts(short iDev, int* nEvents, unsigned long count[])
/// \brief		Get events counts 
/// \details	Allows to get events counts on each inputs. Every seconds values are upated. \n
///				This function can be polled to evaluate the current events apply on HTDC inputs.
///	\note		Device compatibility: all
///
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
/// 
/// \param		nEvents		pointer to the events number(inputs) 
/// 
///	\param		count		pointer to table which contains counting values of each inputs
///	
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///						
short LIB_CALL HTDC_getEventsCounts(short iDev, int* nEvents, unsigned long count[]);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_setDataRate(short iDev, unsigned char iCh, unsigned short value)
/// \brief		Set data rate of target channel(s) measurement
/// \details	Set target channel(s) data rate between 100 to 1000ms. \n
///				Allows to fix the MAX time before to recover data (kind of timeout). 
///				Means, if a big event flow is present on a HTDC input the data transfer 
///				between device and computer is done continuously but if the flow is lower 
///				as the buffer can stored data are sent to host at this data rate.
///				By setting the rate, the user can thus recover the data more or less 
///				quickly using "HTDC_getChXData" or "HTDC_getChXHistogram" functions.		
///	\note		Device compatibility: all
///
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
/// 
/// \param		iCh		index of channel(s): CH_1, CH_2, CH3, CH4 (depends of channels number available)  \n
///						Parameter ORING with others channels id. (ex: HTDC_setDataRate(CH_1|CH_2);)
/// 
///	\param		value	data rate value in ms (between 100 to 1000ms, by step of 100ms)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_setDataRate(short iDev, unsigned char iCh, unsigned short value);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getDataRate(short iDev, unsigned char iCh, unsigned short* value)
/// \brief		Get data rate of a target channel
/// \details	Get current target channel data rate (between 100 to 1000ms).
///				This value corresponds to the MAX time before to recover data (kind of timeout). 
///	\note		Device compatibility: all
///
/// \param		iDev	Device index indicate by "HTDC_listDevices" function				 
///
/// \param		iCh		index of channel(s): CH_1, CH_2, CH3, CH4 (depends of channels number available)
/// 
///	\param		value	return by pointer the data rate value (between 100 to 1000ms, by step of 100ms)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
short LIB_CALL HTDC_getDataRate(short iDev, unsigned char iCh, unsigned short* value);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_setMeasMode(short iDev, unsigned char iCh, unsigned short mode)
/// \brief		Set the measurement mode of target channel(s)
/// \details	Adjust target channel(s) measurement either in "Continuous" or "OneShot" mode.
///				A channel in "Continuous" mode allows to run in free running as soon as the
///				start command is applyed. A channel in "OneShot" mode allows to get one sample
///				of measurement after the start command is send AND ONLY at each 
///				"HTDC_getChXOneShotMeasurement" function sending.
///				By default, all channels measurement mode are in "Continuous" mode.		
///	\note		Device compatibility: all
///				 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iCh		index of channel(s): CH_1, CH_2, CH3, CH4 (depends of channels number available)  \n
///						Parameter ORING with others channels id. (ex: HTDC_setMeasMode(CH_1|CH_2);)
///	\param		mode	measurement mode: 0=Continuous or 1=OneShot
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_setMeasMode(short iDev, unsigned char iCh, unsigned short mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getMeasMode(short iDev, unsigned char iCh, unsigned short* mode)
/// \brief		Get measurement mode of a target channel
/// \details	Get current target channel measurement mode (between "Continuous" or "OneShot" mode). 
///	\note		Device compatibility: all
///				 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// \param		iCh		index of channel(s): CH_1, CH_2, CH3, CH4 (depends of channels number available)  \n
/// 
///	\param		mode	return by pointer the curent measurement mode (0=Continuous or 1=OneShot)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getMeasMode(short iDev, unsigned char iCh, unsigned short* mode);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh1OneShotMeasurement(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count)
/// \brief		Get channel 1 one shot histogram 
/// \details	Get histogram of the data measured from channel 1 during the \n
///				time condition adjusted with the "HTDC_armChannel" function. \n
///				User can set the range of the histogram to recover or let the function \n
///				adjust automatically the best range. As the histogram is represented \n
///				on two axes of 65536 values, the bin width is automatically adjusted to \n
///				respect this deep. \n
///				Respect the correct initialization and order of the functions: \n
///				  * 1: Init the target channel		
///				  * 2: Set channel measurement mode in OneShot	
///				  * 3: Arm the channel with an infinite nSample (-1) and the desired measurement time 
///				  * 4: Start channel
///				  * 5: Run one or more OneShot measurement when you want
///				  * 6: Stop channel
///
///				Example of use:		\n
///				 * HTDC_setChannelConfig(CH_1, 1, 0, 0);		// Init channel: power ON, rising edge, TTL-CMOS
///				 * HTDC_setMeasMode(CH_1, 1);				// Set channel measurement mode to OneShot
///				 * HTDC_armChannel(CH_1, -1, 200);			// Arm channel to recover data during 200ms, no sample number condition 
///				 * HTDC_startChannel(CH_1);					// Start channel
///				 * HTDC_getCh1OneShotMeasurement(0, 0, pHistoX, pHistoY, &bw);	// Get one shot data sample
///				 * HTDC_startChannel(CH_1);					// Stop channel
///
///	\note		This function is a blocking function with a timeout of 30s. 	
///	\note		Device compatibility: all
///				 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///				
/// \param		Xmin		min value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmin=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the min value according \n
///							to the input signal measured.
///
/// \param		Xmax		max value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmax=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the max value according  \n
///							to the input signal measured.
///
/// \param		*histoX		pointer to target buffer for X axis histogram values recovery. \n
///							Each row represent the time value of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*histoY		pointer to target buffer for Y axis histogram values recovery. \n
///							Each row represent the number of occurences of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*binWidth	return by pointer the value of binWidth in ns. \n
///							If the range of the measured signal is upper 65536x13ps (851ns) \n
///							the function automatically adjust the bin width to respect the  \n
///							Y axis deep of maximum 65536 values.	
/// 
/// \param		*count		return by pointer the number of data recovered.		
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh1OneShotMeasurement(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh2OneShotMeasurement(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count)
/// \brief		Get channel 2 one shot histogram 
/// \details	Get histogram of the data measured from channel 2 during the \n
///				time condition adjusted with the "HTDC_armChannel" function. \n
///				User can set the range of the histogram to recover or let the function \n
///				adjust automatically the best range. As the histogram is represented \n
///				on two axes of 65536 values, the bin width is automatically adjusted to \n
///				respect this deep. \n
///				Respect the correct initialization and order of the functions: \n
///				  * 1: Init the target channel		
///				  * 2: Set channel measurement mode in OneShot	
///				  * 3: Arm the channel with an infinite nSample (-1) and the desired measurement time 
///				  * 4: Start channel
///				  * 5: Run one or more OneShot measurement when you want
///				  * 6: Stop channel
///
///				Example of use:		\n
///				 * HTDC_setChannelConfig(CH_2, 1, 0, 0);		// Init channel: power ON, rising edge, TTL-CMOS
///				 * HTDC_setMeasMode(CH_2, 1);				// Set channel measurement mode to OneShot
///				 * HTDC_armChannel(CH_2, -1, 200);			// Arm channel to recover data during 200ms, no sample number condition 
///				 * HTDC_startChannel(CH_2);					// Start channel
///				 * HTDC_getCh2OneShotMeasurement(0, 0, pHistoX, pHistoY, &bw);	// Get one shot data sample
///				 * HTDC_startChannel(CH_2);					// Stop channel
///
///	\note		This function is a blocking function with a timeout of 30s.	
///	\note		Device compatibility: all
///				 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///				
/// \param		Xmin		min value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmin=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the min value according \n
///							to the input signal measured.
///
/// \param		Xmax		max value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmax=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the max value according  \n
///							to the input signal measured.
///
/// \param		*histoX		pointer to target buffer for X axis histogram values recovery. \n
///							Each row represent the time value of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*histoY		pointer to target buffer for Y axis histogram values recovery. \n
///							Each row represent the number of occurences of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*binWidth	return by pointer the value of binWidth in ns. \n
///							If the range of the measured signal is upper 65536x13ps (851ns) \n
///							the function automatically adjust the bin width to respect the  \n
///							Y axis deep of maximum 65536 values.	
/// 
/// \param		*count		return by pointer the number of data recovered.		
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh2OneShotMeasurement(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh3OneShotMeasurement(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count)
/// \brief		Get channel 3 one shot histogram 
/// \details	Get histogram of the data measured from channel 3 during the \n
///				time condition adjusted with the "HTDC_armChannel" function. \n
///				User can set the range of the histogram to recover or let the function \n
///				adjust automatically the best range. As the histogram is represented \n
///				on two axes of 65536 values, the bin width is automatically adjusted to \n
///				respect this deep. \n
///				Respect the correct initialization and order of the functions: \n
///				  * 1: Init the target channel		
///				  * 2: Set channel measurement mode in OneShot	
///				  * 3: Arm the channel with an infinite nSample (-1) and the desired measurement time 
///				  * 4: Start channel
///				  * 5: Run one or more OneShot measurement when you want
///				  * 6: Stop channel
///
///				Example of use:		\n
///				 * HTDC_setChannelConfig(CH_3, 1, 0, 0);		// Init channel: power ON, rising edge, TTL-CMOS
///				 * HTDC_setMeasMode(CH_3, 1);				// Set channel measurement mode to OneShot
///				 * HTDC_armChannel(CH_3, -1, 200);			// Arm channel to recover data during 200ms, no sample number condition 
///				 * HTDC_startChannel(CH_3);					// Start channel
///				 * HTDC_getCh3OneShotMeasurement(0, 0, pHistoX, pHistoY, &bw);	// Get one shot data sample
///				 * HTDC_startChannel(CH_3);					// Stop channel
///
///	\note		This function is a blocking function with a timeout of 30s. 
///	\note		Device compatibility: all
///				 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///				
/// \param		Xmin		min value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmin=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the min value according \n
///							to the input signal measured.
///
/// \param		Xmax		max value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmax=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the max value according  \n
///							to the input signal measured.
///
/// \param		*histoX		pointer to target buffer for X axis histogram values recovery. \n
///							Each row represent the time value of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*histoY		pointer to target buffer for Y axis histogram values recovery. \n
///							Each row represent the number of occurences of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*binWidth	return by pointer the value of binWidth in ns. \n
///							If the range of the measured signal is upper 65536x13ps (851ns) \n
///							the function automatically adjust the bin width to respect the  \n
///							Y axis deep of maximum 65536 values.			
/// 
/// \param		*count		return by pointer the number of data recovered.
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh3OneShotMeasurement(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_getCh4OneShotMeasurement(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count)
/// \brief		Get channel 4 one shot histogram 
/// \details	Get histogram of the data measured from channel 4 during the \n
///				time condition adjusted with the "HTDC_armChannel" function. \n
///				User can set the range of the histogram to recover or let the function \n
///				adjust automatically the best range. As the histogram is represented \n
///				on two axes of 65536 values, the bin width is automatically adjusted to \n
///				respect this deep. \n
///				Respect the correct initialization and order of the functions: \n
///				  * 1: Init the target channel		
///				  * 2: Set channel measurement mode in OneShot	
///				  * 3: Arm the channel with an infinite nSample (-1) and the desired measurement time 
///				  * 4: Start channel
///				  * 5: Run one or more OneShot measurement when you want
///				  * 6: Stop channel
///
///				Example of use:		\n
///				 * HTDC_setChannelConfig(CH_4, 1, 0, 0);		// Init channel: power ON, rising edge, TTL-CMOS
///				 * HTDC_setMeasMode(CH_4, 1);				// Set channel measurement mode to OneShot
///				 * HTDC_armChannel(CH_4, -1, 200);			// Arm channel to recover data during 200ms, no sample number condition 
///				 * HTDC_startChannel(CH_4);					// Start channel
///				 * HTDC_getCh4OneShotMeasurement(0, 0, pHistoX, pHistoY, &bw);	// Get one shot data sample
///				 * HTDC_startChannel(CH_4);					// Stop channel
///
///	\note		This function is a blocking function with a timeout of 30s.
///	\note		Device compatibility: all
///				 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///				
/// \param		Xmin		min value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmin=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the min value according \n
///							to the input signal measured.
///
/// \param		Xmax		max value in ns of the histogram to recover.\n
///							Range: 0 to 1000000000ns (1s) \n
///							Note: if Xmax=0, the histogram is in auto scale mode. \n
///							Means the function will adjust the max value according  \n
///							to the input signal measured.
///
/// \param		*histoX		pointer to target buffer for X axis histogram values recovery. \n
///							Each row represent the time value of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*histoY		pointer to target buffer for Y axis histogram values recovery. \n
///							Each row represent the number of occurences of bin. \n
///							Note: the target buffer size must be of 65536. \n
///
/// \param		*binWidth	return by pointer the value of binWidth in ns. \n
///							If the range of the measured signal is upper 65536x13ps (851ns) \n
///							the function automatically adjust the bin width to respect the  \n
///							Y axis deep of maximum 65536 values.		
/// 
/// \param		*count		return by pointer the number of data recovered.
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL HTDC_getCh4OneShotMeasurement(short iDev, double Xmin, double Xmax, double* histoX, double* histoY, double* binWidth, long* count);
/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_Set_All_Save_In_Factory_Settings(short iDev)
/// \brief		Set All Save in factory Settings (Default Parameter)
/// \details	Configure the HTDC parameter by default on Save1, Current save
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_Set_All_Save_In_Factory_Settings(short iDev);
/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_Set_Factory_Settings(short iDev)
/// \brief		Set Current in factory Settings (Default Parameter) but Save1 keep these save parameter
/// \details	Configure the HTDC parameter by default on Current save only. You keep your Save1 save with these proper parameter
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_Set_Factory_Settings(short iDev);
/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_Set_Save1_All_Settings(short iDev)
/// \brief		Set the Current Parameter in your User save's Save1
/// \details	Configure the User save's Save1 with the current parameter you use and load it in Memory
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_Set_Save1_All_Settings(short iDev);
/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_Set_Save1_Others_Settings(short iDev)
/// \brief		Set the Current Parameter in your User save's Save1 for Others Setinngs
/// \details	Configure the User save's Save1 with the current parameter you use and load it in Memory, only for others Settings (Sync, Channel, Measure)
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_Set_Save1_Others_Settings(short iDev);
/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_Set_Reset_Save1_All_Settings(short iDev)
/// \brief		Reset the Save1 Save for all Settings
/// \details	Reset the user save's Save1 for all parameter in Default values (factory settings)
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_Set_Reset_Save1_All_Settings(short iDev);
/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_Set_Reset_Save1_Others_Settings(short iDev)
/// \brief		Reset the Save1 Save for others Settings
/// \details	Reset the user save's Save1 for others parameters (Sync, Channel, Measure) in Default values (factory settings)
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_Set_Reset_Save1_Others_Settings(short iDev);
/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_Apply_All_Save1_Settings_In_Current(short iDev)
/// \brief		Apply Save1 save in Current Save
/// \details	Configure the Current save with the User's save Save1 and load the new Current Parameter it in Memory
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_Apply_All_Save1_Settings_In_Current(short iDev);
/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short HTDC_Apply_All_Save1_Others_Settings_In_Current(short iDev)
/// \brief		Apply Save1 save in Current Save for Others Settings
/// \details	Configure the Current save with the User's save Save1 you use and load it in Current Memory, only for others Settings (Sync, Channel, Measure)
///	\note		Device compatibility: PN_HTDC_Mx_S
/// 
/// \param		iDev	Device index indicate by "HTDC_listDevices" function
///
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///	
short LIB_CALL HTDC_Apply_All_Save1_Others_Settings_In_Current(short iDev);
/////////////////////////////////////////////////////////////////////////////////////


#if defined(__cplusplus)
}
#endif

#endif
