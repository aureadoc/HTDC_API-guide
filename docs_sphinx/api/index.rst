.. _api:

All Functions
=============

This section provides the prototypes and descriptions of all functions    
integrated into HTDC library.

.. warning::

   More or less functions are available according to the device type.  
   The compatibility depends on the device part number recovered by
   the "HTDC_getSystemVersion" function.                        
   Please see notes functions to check the compatibility with your device:    
   -> Device compatibility: PN_HTDC_Mx_x  

.. toctree::
   :maxdepth: 2
   :glob:

   *