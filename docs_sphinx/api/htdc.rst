.. _api_functions:


Library information
===================

HTDC_getLibVersion
------------------

.. doxygenfunction:: HTDC_getLibVersion
   :project: HTDC - API guide




Connection Functions
====================

HTDC_listDevices
----------------

.. doxygenfunction:: HTDC_listDevices
   :project: HTDC - API guide

HTDC_openDevice
---------------

.. doxygenfunction:: HTDC_openDevice
   :project: HTDC - API guide

HTDC_closeDevice
----------------

.. doxygenfunction:: HTDC_closeDevice
   :project: HTDC - API guide





Device Information
==================

HTDC_getSystemVersion
---------------------

.. doxygenfunction:: HTDC_getSystemVersion
   :project: HTDC - API guide

HTDC_getSystemFeature
---------------------

.. doxygenfunction:: HTDC_getSystemFeature
   :project: HTDC - API guide





Set and Get HTDC Configuration
==============================

HTDC_setSyncSource
------------------

.. doxygenfunction:: HTDC_setSyncSource
   :project: HTDC - API guide

HTDC_getSyncSource
------------------

.. doxygenfunction:: HTDC_getSyncSource
   :project: HTDC - API guide

HTDC_setInternalSyncFrequency
-----------------------------

.. doxygenfunction:: HTDC_setInternalSyncFrequency
   :project: HTDC - API guide

HTDC_getInternalSyncFrequency
-----------------------------

.. doxygenfunction:: HTDC_getInternalSyncFrequency
   :project: HTDC - API guide

HTDC_setSyncDivider
-------------------

.. doxygenfunction:: HTDC_setSyncDivider
   :project: HTDC - API guide

HTDC_getSyncDivider
-------------------

.. doxygenfunction:: HTDC_getSyncDivider
   :project: HTDC - API guide

HTDC_setSyncInputConfig
-----------------------

.. doxygenfunction:: HTDC_setSyncInputConfig
   :project: HTDC - API guide

HTDC_getSyncInputConfig
-----------------------

.. doxygenfunction:: HTDC_getSyncInputConfig
   :project: HTDC - API guide

HTDC_setResultFormat
--------------------

.. doxygenfunction:: HTDC_setResultFormat
   :project: HTDC - API guide

HTDC_getResultFormat
--------------------

.. doxygenfunction:: HTDC_getResultFormat
   :project: HTDC - API guide






Set and Get Channel Configuration
=================================

HTDC_setChannelDelay
--------------------

.. doxygenfunction:: HTDC_setChannelDelay
   :project: HTDC - API guide

HTDC_getChannelDelay
--------------------

.. doxygenfunction:: HTDC_getChannelDelay
   :project: HTDC - API guide

HTDC_setChannelConfig
---------------------

.. doxygenfunction:: HTDC_setChannelConfig
   :project: HTDC - API guide

HTDC_getChannelConfig
---------------------

.. doxygenfunction:: HTDC_getChannelConfig
   :project: HTDC - API guide

HTDC_setDataRate
----------------

.. doxygenfunction:: HTDC_setDataRate
   :project: HTDC - API guide

HTDC_getDataRate
----------------

.. doxygenfunction:: HTDC_getDataRate
   :project: HTDC - API guide

HTDC_setMeasMode
----------------

.. doxygenfunction:: HTDC_setMeasMode
   :project: HTDC - API guide

HTDC_getMeasMode
----------------

.. doxygenfunction:: HTDC_getMeasMode
   :project: HTDC - API guide




Channel State
=============

HTDC_armChannel
---------------

.. doxygenfunction:: HTDC_armChannel
   :project: HTDC - API guide

HTDC_startChannel
-----------------

.. doxygenfunction:: HTDC_startChannel
   :project: HTDC - API guide

HTDC_stopChannel
----------------

.. doxygenfunction:: HTDC_stopChannel
   :project: HTDC - API guide






Monitoring Functions
====================

HTDC_getChannelState
--------------------

.. doxygenfunction:: HTDC_getChannelState
   :project: HTDC - API guide

HTDC_getEventsCounts
--------------------

.. doxygenfunction:: HTDC_getEventsCounts
   :project: HTDC - API guide

HTDC_getCh1Data
---------------

.. doxygenfunction:: HTDC_getCh1Data
   :project: HTDC - API guide

HTDC_getCh2Data
---------------

.. doxygenfunction:: HTDC_getCh2Data
   :project: HTDC - API guide

HTDC_getCh3Data
---------------

.. doxygenfunction:: HTDC_getCh3Data
   :project: HTDC - API guide

HTDC_getCh4Data
---------------

.. doxygenfunction:: HTDC_getCh4Data
   :project: HTDC - API guide

HTDC_getCh1Histogram
--------------------

.. doxygenfunction:: HTDC_getCh1Histogram
   :project: HTDC - API guide

HTDC_getCh2Histogram
--------------------

.. doxygenfunction:: HTDC_getCh2Histogram
   :project: HTDC - API guide

HTDC_getCh3Histogram
--------------------

.. doxygenfunction:: HTDC_getCh3Histogram
   :project: HTDC - API guide

HTDC_getCh4Histogram
--------------------

.. doxygenfunction:: HTDC_getCh4Histogram
   :project: HTDC - API guide

HTDC_getCh1OneShotMeasurement
-----------------------------

.. doxygenfunction:: HTDC_getCh1OneShotMeasurement
   :project: HTDC - API guide

HTDC_getCh2OneShotMeasurement
-----------------------------

.. doxygenfunction:: HTDC_getCh2OneShotMeasurement
   :project: HTDC - API guide

HTDC_getCh3OneShotMeasurement
-----------------------------

.. doxygenfunction:: HTDC_getCh3OneShotMeasurement
   :project: HTDC - API guide

HTDC_getCh4OneShotMeasurement
-----------------------------

.. doxygenfunction:: HTDC_getCh4OneShotMeasurement
   :project: HTDC - API guide






Cross Correlation
=================

HTDC_setCrossCorrelationALU
---------------------------

.. doxygenfunction:: HTDC_setCrossCorrelationALU
   :project: HTDC - API guide

HTDC_getCrossCorrelationData
----------------------------

.. doxygenfunction:: HTDC_getCrossCorrelationData
   :project: HTDC - API guide