.. _example:

Code Examples
=============

The following section present simple codes in C++ and Python to use ChronoXea device.

Communication
-------------

This first example shows how to list all ChronoXea connected to a computer and how to open and close USB communication. Device information is also recovered in this example.

C++ program
~~~~~~~~~~~

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "HTDC.h"

	////////////////////////////////////////
	// Channels number available
	#define HTDC_N_CH	4

	// Number of maximum data to recover 
	#define DATA_MAX	1000000
	////////////////////////////////////////

	int main(int argc, const char* argv[]) {
		short iDev = 0;
		short ret;
		char* devicesList[10];
		short numberDevices;
		char* pch;
		char* next_pch = NULL;
		char version[64];
		char versionParam[3][32];
		char systemName[6];
		memset(version, ' ', 64);
		memset(systemName, '\0', 6);

		/*	listDevices function	*/
		// List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
		if (HTDC_listDevices(devicesList, &numberDevices) == 0) {
			if (numberDevices == 0) {
				cout << endl << "	Please connect device !" << endl << endl;
				do {
					delay(500);
					HTDC_listDevices(devicesList, &numberDevices);
				} while (numberDevices == 0);
			}
		}

		// If multiple HTDC devices are detected, select one else open it
		if (numberDevices > 1) {
			for (int i = 0; i < numberDevices; i++) {
				printf(" -%u: %s\n", i, devicesList[i]);
			}
			cout << endl << "Select device to drive: ";
			cin >> iDev;

			if (HTDC_openDevice(iDev) != 0) {
				cout << "Failed to open HTDC" << endl;
			}
		}
		else {
			iDev = 0;
			if (HTDC_openDevice(iDev) != 0) {
				cout << "Failed to open HTDC" << endl;
			}
		}

		// Recovery of the system version
		if (HTDC_getSystemVersion(iDev, version) == 0) {
			cout << endl << "  * Hardware-Firmware version:" << endl << endl;
		}
		else {
			cout << endl << " -> Failed to get system version" << endl << endl;
		}

		// Loop to extract HTDC parameters 
		int v = 0;
		pch = secure_strtok(version, ":", &next_pch);
		while (pch != NULL) {
			snprintf((char*)&versionParam[v][0], 32, "%s", pch);
			pch = secure_strtok(NULL, ":", &next_pch);
			v++;
		}
		if (pch != 0) { snprintf((char*)&versionParam[v][0], 32, "%s", pch); }

		// Display HTDC information
		memcpy(systemName, (char*)&versionParam[2][0] + 3, 3);
		cout << "	AT System       : " << systemName << endl;
		cout << "	Serial number   : " << versionParam[0] << endl;
		cout << "	Product number  : " << versionParam[1] << endl;
		cout << "	Firmware version: " << versionParam[2] << endl;
		cout << endl;

		// Wait some time
		delay(2000);

		/*	CloseDevice function	*/
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (HTDC_closeDevice(iDev) == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;

		return 0;
	}

Python program
~~~~~~~~~~~~~~

.. code-block:: python

	from ctypes import *
	import time

	# Import HTDC wrapper file 
	import HTDC_wrapper as ChronoXea 

	# Application main
	def main():
		key = ''
		iDev = c_short(0)
		nDev = c_short()
		devList = []

		# Scan and open selected device
		devList,nDev=ChronoXea.listDevices()
		if nDev==0:   # if no device detected, wait
			print ("No device connected, waiting...")
			while nDev==0:
		    	devList,nDev=ChronoXea.listDevices()
		    	time.sleep(1)
		elif nDev>1:  # if more 1 device detected, select target
			print("Found " + str(nDev) + " device(s) :")
			for i in range(nDev):
		    	print (" -"+str(i)+": " + devList[i])
			iDev=int(input("Select device to open (0 to n):")) 

		# Open device
		if ChronoXea.openDevice(iDev)<0:
			input(" -> Failed to open device, press enter to quit !")
			return 0	
		print("Device correctly opened")

		# Recover system version
		ret,version = ChronoXea.getSystemVersion(iDev)
		if ret<0: print(" -> failed\n")
		else:print("System version = {} \n".format(version))

		# Wait some time
		time.sleep(2)

		# Close device communication
		ChronoXea.closeDevice(iDev)

	# Python main entry point
	if __name__ == "__main__":
		main() 


Single Channel Measurement
--------------------------

The next example shows how to use ChronoXea to make a measurement on a single channel.

C++ program
~~~~~~~~~~~

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "HTDC.h"

	////////////////////////////////////////
	// Target channel
	#define TARGET_CH	CH_1

	// Number sample time to recover
	#define N_SAMPLE	100000
	////////////////////////////////////////

	// Input edge
	#define RISING_EDGE	0
	#define FALLING_EDGE	1

	// input state
	#define ON	1
	#define OFF	0

	// Input level
	#define TTL_CMOS	0
	#define	NIM			1

	// Sync source
	#define EXTERNAL_SYNC	0
	#define INTERNAL_SYNC	1

	// Measurement mode
	#define CONTINUOUS_MODE 0
	#define ONESHOTE_MODE	1

	int main(int argc, const char* argv[]) {
		char* devicesList[10];
		short numberDevices, iDev;
		unsigned long long nData = 0;
		unsigned long n;
		unsigned long nSampleToRecover, nSampleRecovered = 0;
		int status;
		short ret;
		short system_chNumber, system_integrationMode;

		// Memory allocation to recover data from HTDC
		// Offset of 65535 mandatory for exceeding data
		unsigned long long* data;
		data = (unsigned long long*)malloc(N_SAMPLE + 65536);

		if (data == NULL) {
			printf(" --> Failed to assign memory ! \n");
			system("pause");
			return -1;
		}

		// Loop to list devices until at least one is founded
		ret = HTDC_listDevices(devicesList, &numberDevices);
		if (ret == 0) {
			if (numberDevices == 0) {
				cout << endl << "Please connect AT device !" << endl << endl;
				do {
					delay(500);
					ret = HTDC_listDevices(devicesList, &numberDevices);
				} while (numberDevices == 0);
			}
		}

		// If multiple HTDC devices are detected, select one else open it
		if (numberDevices > 1) {
			for (int i = 0; i < numberDevices; i++) {
				printf(" -%u: %s\n", i, devicesList[i]);
			}
			cout << endl << "Select device to drive: ";
			cin >> iDev;

			if (HTDC_openDevice(iDev) == 0) {
				cout << "HTDC " << iDev << " correctly open !";
			}
			else cout << "Failed to open HTDC" << endl;
		}
		else {
			iDev = 0;
			if (HTDC_openDevice(iDev) == 0) {
				cout << "HTDC " << iDev << " correctly open !";
			}
			else cout << "Failed to open HTDC" << endl;
		}

		// Get system features
		//--------------------

		// Get the number of channels available
		HTDC_getSystemFeature(iDev, 0, &system_chNumber);

		// Get the system integration mode
		HTDC_getSystemFeature(iDev, 1, &system_integrationMode);

		// Adjust Sync divider to 1
		cout << "Set Sync divider" << endl;
		if (HTDC_setSyncDivider(iDev, 1) != 0)
			cout << " -> Failed " << endl;
		else
			cout << " -> Done" << endl;

		// Configure Sync input : enable, rising edge and TTL-CMOS level
		cout << "Set input Sync" << endl;
		if (HTDC_setSyncInputConfig(iDev, ON, RISING_EDGE, TTL_CMOS) != 0)
			cout << " -> Failed " << endl;
		else
			cout << " -> Done" << endl;

		// Apply a 5.5ns delay on target channel 
		cout << "Set channel delay" << endl;
		if (HTDC_setChannelDelay(iDev, TARGET_CH, 5.5) != 0)
			cout << " -> Failed " << endl;
		else
			cout << " -> Done" << endl;

		// Activate target channel input: power ON and rising edge
		cout << "Channel activation" << endl;
		if (HTDC_setChannelConfig(iDev, TARGET_CH, ON, RISING_EDGE, TTL_CMOS) != 0)
			cout << " -> Failed " << endl;
		else
			cout << " -> Done" << endl;

		// Set channel measurement mode to Continuous
		cout << "Set channel measurement mode" << endl;
		if (HTDC_setMeasMode(iDev, TARGET_CH, CONTINUOUS_MODE) != 0)
			cout << " -> Failed" << endl;
		else
			cout << " -> Done" << endl;

		// Arm target channel: need of 10000 data during infinite time
		cout << "Arm channel" << endl;
		nSampleToRecover = 10000;
		if (HTDC_armChannel(iDev, TARGET_CH, nSampleToRecover, -1) != 0)
			cout << " -> Failed" << endl;
		else
			cout << " -> Done" << endl;

		// Waiting for Sync signal stabilization...
		cout << "Waiting for sync signal stabilization..." << endl;
		delay(3000);

		// Measurement
		//------------

		// Start target channel
		cout << "Start channel" << endl;
		if (HTDC_startChannel(iDev, TARGET_CH) != 0)
			cout << " -> Failed" << endl;
		else
			cout << " -> Done" << endl;

		// Recover data 
		cout << "Recover target channel data... " << endl;
		while (nSampleRecovered < nSampleToRecover) {
			// Recover target channel state, to known how much data are available
			HTDC_getChannelState(iDev, TARGET_CH, &status, &nSampleToRecover, &nSampleRecovered);

			// Store sample data
			switch (TARGET_CH) {
				//case CH_1: if (HTDC_getCh1Data(iDev, data, &n) != 0) { cout << "Channel 1 : Failed to recover data !" << endl; } break;
			case CH_1: if (HTDC_getCh1Data(iDev, data + nData, &n) == 0) { nData += n; } break;
			case CH_2: if (HTDC_getCh2Data(iDev, data + nData, &n) == 0) { nData += n; } break;
			case CH_3: if (HTDC_getCh3Data(iDev, data + nData, &n) == 0) { nData += n; } break;
			case CH_4: if (HTDC_getCh4Data(iDev, data + nData, &n) == 0) { nData += n; } break;
			}

			delay(100);
			printf("\r %lu/%lu data recovered", nSampleRecovered, nSampleToRecover);
		}

		// Stop target channel
		cout << "Stop channel" << endl;
		if (HTDC_stopChannel(iDev, TARGET_CH) != 0)
			cout << " -> Failed" << endl;
		else
			cout << " -> Done" << endl;

		// Display sample result
		cout << "\nSample time recovered:" << endl;
		for (int i = 0; i<int(nData); i++) {
			printf(" sample[%i]= %1.3fns \n", i, data[i] * HTDC_RES);
			if (i > 10) break;
		}

		// Wait some time
		delay(2000);

		// Close device communication
		HTDC_closeDevice(iDev);
		return 0;
	}


Python program
~~~~~~~~~~~~~~

.. code-block:: python

	from ctypes import *
	import time

	# Import HTDC wrapper file 
	import HTDC_wrapper as ChronoXea  

	# Application main
	def main():
		iDev=int(0)
		nDev=c_short()
		devList=[]
		nSampleRecovered=int(0)
		nSampleToRecover=int(0)
		sampleList=[]

		# List and display avaliable devices
		devList,nDev=ChronoXea.listDevices()
		if nDev==0:   # if no device detected, wait
			print ("No device connected, waiting...")
			while nDev==0:
		    	devList,nDev=ChronoXea.listDevices()
		    	time.sleep(1)
		elif nDev>1:  # if more 1 device detected, select target
			print("Found " + str(nDev) + " device(s) :")
			for i in range(nDev):
		    	print (" -"+str(i)+": " + devList[i])
			iDev=int(input("Select device to open (0 to n):")) 

		# Open device
		if ChronoXea.openDevice(iDev)<0:
			input(" -> Failed to open device, press enter to quit !")
			return 0	
		print("Device correctly opened")

		# Set sync source: in internal
		print("Sync source")
		ret = ChronoXea.setSyncSource(iDev, 1)
		if ret == 0:
			print("\nSync Source Set\n")
		else:
			print("\nset Sync Source: error\n")

		# Set internal sync frequency to 10kHz
		print("Internal Sync frequency")
		ret = ChronoXea.setInternalSyncFrequency(iDev, 10000)
		if ret == 0:
			print("\nInternal Sync frequency Set\n")
		else:
			print("\nset Internal Sync frequency: error\n")

		# Set sync divider to 1
		print("Sync divier")
		ret = ChronoXea.setSyncDivider(iDev, 1)
		if ret == 0:
			print("\nSync Divider Set\n")
		else:
			print("\nset Sync Divider: error\n")

		# Set sync input configuration: Enable, rising edge and TTL-CMOS level
		print("Set input Sync")
		ret = ChronoXea.setSyncInputConfig(iDev,1,0,0)
		if ret == 0:
			print("\nSync Input Config Set\n")
		else:
			print("\nset Sync Input Config: error\n")

		# Set target channel delay to 5.5ns
		print("Sync channel delay")
		ret = ChronoXea.setChannelDelay(iDev, TARGET_CH, 5.5)
		if ret == 0:
			print("\nChannel Delay Set\n")
		else:
			print("\nset Channel delay: error\n")

		# Set channel(s) configuration: Power ON, rising edge and TTL-CMOS level
		print("Channel configuration")
		ret = ChronoXea.setChannelConfig(iDev, TARGET_CH, 1, 0, 0)
		if ret == 0:
			print("\nChannel config Set\n")
		else:
			print("\nset Channel config: error\n")

		# Arm channel(s): arm target channel to recover N_SAMPLE
		print("Arm channel")
		ret = ChronoXea.armChannel(iDev,TARGET_CH,N_SAMPLE)
		if ret == 0:
			print("\nChannel Armed\n")
		else:
			print("\nArm Channel: error\n")
		nSampleToRecover=N_SAMPLE

		print("Waiting stable sync signal... ")
		time.sleep(5)

		# Start channel(s): start target channel
		print("Start channel")
		ret = ChronoXea.startChannel(iDev, TARGET_CH)
		if ret == 0:
			print("\nChannel Started\n")
		else:
			print("\nStart Channel: error\n")

		# Recover data
		print("Recover target channel data... ")
		while nSampleRecovered<nSampleToRecover:

			# Recover target channel state, to known how much data are available
			ret, state,nSampleToRecover,nSample = ChronoXea.getChannelState(iDev, TARGET_CH)
			if(ret == 0):
			    nSampleRecovered += nSample

			    # Get channel data
			    ret,n,sample = ChronoXea.getChannelData(iDev, TARGET_CH)
			    if ret==0:
			        # Store result if data available
			        if n>0: sampleList+=sample

			        # Wait and display progression
			        time.sleep(0.5)
			        print("\r State: {} | {}/{} data recovered".format(state,nSampleRecovered,nSampleToRecover))
			    else:
			        print("\nGet Channel Data: error\n")
			else:
			    print("\nchannel State: error\n")

		# Display part of data recovered
		print("\nSample time recovered:")
		for i in range (10):
			print(" sample[{}]={}ns".format(i,round(sampleList[i]*ChronoXea.HTDC_RES,3)))

		print("\nEnd of program")

		# Close device
		ChronoXea.closeDevice(iDev)

	# Python main entry point
	if __name__ == "__main__":
		main() 

.. note::

	All function information is available in section :ref:`All Functions`.


OneShot Measurement
--------------------

The next example shows how to use ChronoXea to make a One Shot Measurement

C++ program
~~~~~~~~~~~

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "HTDC.h"

	////////////////////////////////////////
	// Target channel
	#define TARGET_CH	CH_1

	// Number shot to apply
	#define N_SHOT	10

	// Measurement time in ms (between 100 to 100ms)
	#define MEAS_TIME	200

	// Deadtime betwwen 2 shot in ms
	#define MEAS_DEADTIME	100

	// Number sample time to recover
	#define N_SAMPLE	100000
	////////////////////////////////////////

	// Input edge
	#define RISING_EDGE	0
	#define FALLING_EDGE	1

	// input state
	#define ON	1
	#define OFF	0

	// Input level
	#define TTL_CMOS	0
	#define	NIM			1

	// Sync source
	#define EXTERNAL_SYNC	0
	#define INTERNAL_SYNC	1

	// Measurement mode
	#define CONTINUOUS_MODE 0
	#define ONESHOTE_MODE	1

	int main(int argc, const char* argv[]) {
		char* devicesList[10];
		short numberDevices, iDev;
		long n;
		double bw;
		short ret;
		short system_chNumber, system_integrationMode;
		double* pHistoX[N_SHOT];
		double* pHistoY[N_SHOT];

		/*	listDevices function	*/
		// List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
		if (HTDC_listDevices(devicesList, &numberDevices) == 0) {
			if (numberDevices == 0) {
				cout << endl << "	Please connect device !" << endl << endl;
				do {
					delay(500);
					HTDC_listDevices(devicesList, &numberDevices);
				} while (numberDevices == 0);
			}
		}

		// If multiple HTDC devices are detected, select one else open it
		if (numberDevices > 1) {
			for (int i = 0; i < numberDevices; i++) {
				printf(" -%u: %s\n", i, devicesList[i]);
			}
			cout << endl << "Select device to drive: ";
			cin >> iDev;

			if (HTDC_openDevice(iDev) == 0) {
				cout << "HTDC " << iDev << " correctly open !";
			}
			else cout << "Failed to open HTDC" << endl;
		}
		else {
			iDev = 0;
			if (HTDC_openDevice(iDev) == 0) {
				cout << "HTDC " << iDev << " correctly open !";
			}
			else cout << "Failed to open HTDC" << endl;
		}

		// Get system features
		//--------------------

		// Get the number of channels available
		HTDC_getSystemFeature(iDev, 0, &system_chNumber);

		// Get the system integration mode
		HTDC_getSystemFeature(iDev, 1, &system_integrationMode);

		// Adjust Sync divider to 1
		cout << "Set Sync divider" << endl;
		if (HTDC_setSyncDivider(iDev, 1) != 0)
			cout << " -> Failed " << endl;
		else
			cout << " -> Done" << endl;

		// Configure Sync input : enable, rising edge and TTL-CMOS level
		cout << "Set input Sync" << endl;
		if (HTDC_setSyncInputConfig(iDev, ON, RISING_EDGE, TTL_CMOS) != 0)
			cout << " -> Failed " << endl;
		else
			cout << " -> Done" << endl;

		// Activate target channel input: power ON and rising edge
		cout << "Channel activation" << endl;
		if (HTDC_setChannelConfig(iDev, TARGET_CH, ON, RISING_EDGE, TTL_CMOS) != 0)
			cout << " -> Failed " << endl;
		else
			cout << " -> Done" << endl;

		// Set channel measurement mode to Continuous
		cout << "Set channel measurement mode" << endl;
		if (HTDC_setMeasMode(iDev, TARGET_CH, ONESHOTE_MODE) != 0)
			cout << " -> Failed" << endl;
		else
			cout << " -> Done" << endl;

		// Arm target channel: need of 10000 data during infinite time
		cout << "Arm channel" << endl;
		if (HTDC_armChannel(iDev, TARGET_CH, -1, MEAS_TIME) != 0)
			cout << " -> Failed" << endl;
		else
			cout << " -> Done" << endl;

		// Waiting for Sync signal stabilization...
		cout << "Waiting for sync signal stabilization..." << endl;
		delay(3000);


		// Measurement
		//------------

		// Start target channel
		cout << "Start channel" << endl;
		if (HTDC_startChannel(iDev, TARGET_CH) != 0)
			cout << " -> Failed" << endl;
		else
			cout << " -> Done" << endl;

		// Recover the N shots and store data
		for (int i = 0; i < N_SHOT; i++)
		{
			cout << "One shot " << i << endl;
			if (TARGET_CH & 1) if (HTDC_getCh1OneShotMeasurement(iDev, 0, 0, pHistoX[i], pHistoY[i], &bw, &n) != 0) { cout << " Channel 1 : Failed to recover OneShoot Data" << endl; }
			if (TARGET_CH & 2) if (HTDC_getCh2OneShotMeasurement(iDev, 0, 0, pHistoX[i], pHistoY[i], &bw, &n) != 0) { cout << " Channel 2 : Failed to recover OneShoot Data" << endl; }
			if (TARGET_CH & 4) if (HTDC_getCh3OneShotMeasurement(iDev, 0, 0, pHistoX[i], pHistoY[i], &bw, &n) != 0) { cout << " Channel 3 : Failed to recover OneShoot Data" << endl; }
			if (TARGET_CH & 8) if (HTDC_getCh4OneShotMeasurement(iDev, 0, 0, pHistoX[i], pHistoY[i], &bw, &n) != 0) { cout << " Channel 4 : Failed to recover OneShoot Data" << endl; }
			cout << " -> data recovered = " << n << endl;
			delay(MEAS_DEADTIME);
		}

		// Stop target channel
		cout << "Stop channel" << endl;
		if (HTDC_stopChannel(iDev, TARGET_CH) != 0)
			cout << " -> Failed" << endl;
		else
			cout << " -> Done" << endl;

		// Wait some time
		delay(2000);

		/*	CloseDevice function	*/
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (HTDC_closeDevice(iDev) == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;

		return 0;
	}
