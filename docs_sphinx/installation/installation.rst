.. _installation:

Software Installation Guide
===========================

The following section describes how to install the ChronoXea software on Windows, MacOS and Linux operating systems.

Windows
-------

Operating Systems Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	- Windows 7 or higher

	- Application and examples are working on 32bit and 64bit systems.
	

Installation Step
^^^^^^^^^^^^^^^^^

	#. Run the setup file locate in provided directory.

	#. Connect ChronoXea device to your computer with the USB cable.

	#. Start Aurea-ChronoXea application or start Aurea-Launcher and then click on your device to use the software. 


MacOS
-----

	- Aurea-Launcher Installation :

		#. Double click on Aurea-Launcher.dmg file.

		#. Drag Aurea-Launcher in the Applications folder.

	- Aurea-ChronoXea Installation :

		#. Double click on Aurea-ChronoXea.dmg file.

		#. Drag Aurea-ChronoXea in the Applications folder

		#. Connect ChronoXea device to your computer with the USB cable.

		#. Launch Aurea-ChronoXea or Aurea-Launcher application by clicking on it.

Linux
-----
	
	- Aurea-Launcher Installation :

		#. Unzip Aurea-Launcher-package.zip

		#. Go to Aurea-Launcher-package/Aurea-Launcher and double-click on Aurea-Launcher-Installer.

		#. Follow the installer instructions and make sure to install all Aurea Technology software in the same directory.

	- Aurea-ChronoXea Installation :

		#. Unzip Aurea-ChronoXea-package.zip

		#. Go to Aurea-ChronoXea-package/Aurea-ChronoXea and double-click on Aurea-ChronoXea-Installer.

		#. Follow the installer instructions and make sure to install all Aurea Technology software in the same directory.

		#. Connect ChronoXea device to your computer with the USB cable.

		#. Launch Aurea-ChronoXea or Aurea-Launcher application by executing the following command in the installation directory.

	.. code-block:: console

	    ./Aurea-Launcher.sh

	.. code-block:: console

	    ./Aurea-ChronoXea.sh