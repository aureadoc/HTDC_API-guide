.. _revision :

Revision History
================

v2.05 (29/10/24)
----------------
- Add a save for user parameters
- Add a factory parameter settings command

v2.04 (27/02/24)
----------------
- Cap sync internal clock to 25 MHz for tag only usage
        
v2.03 (21/12/23)
----------------
- Cap divider to 511, according to IC maximum value

v2.02 (30/08/23)
----------------
- Add Python functions	

v2.01 (24/11/22)
----------------

- Change libusb interface index to open multiple devices
				
v2.00 (05/05/22)
----------------

- Add Device index in all functions
- Modify all get function to return value by pointer
- Replace ``TDC_`` by ``HTDC_`` at the beginning of all functions
- Add Wrapper
- Handle multiple device application

v1.11 (14/04/22)
----------------

- Add internal mutex

v1.10 (15/01/22)
----------------

- Add mutex to avoid problems

v1.09 (15/10/21)
----------------

- Improve range to 7s

v1.08 (24/07/21)
----------------

- Fix erroneous time tag data 

v1.07 (10/05/21)
----------------

 - Improve internal data process
 - Modify comments for 'TDC_getCrossCorrelationData' function

v1.06 (15/03/21)
----------------

- Improve functions
	- TDC_armCh : allows inter-correlation using 
- Add comments instruction for 'TDC_getCrossCorrelationData' function

v1.05 (26/02/21)
----------------

- Improve function :
	- TDC_getChxData : return correct n data according to consign
- Add comments on functions header

v1.04 (13/10/20)
----------------

- Add function :
	- TDC_getSystemVersion
	- TDC_getSystemFeature

v1.03 (01/06/20)
----------------

- Modify functions :
	- TDC_armChannel : add time condition
- Add functions :
	- TDC_setDataRate
	- TDC_getDataRate
	- TDC_setMeasMode
	- TDC_getMeasMode
	- TDC_getCh1OneShotMeasurement
	- TDC_getCh2OneShotMeasurement
	- TDC_getCh3OneShotMeasurement
	- TDC_getCh4OneShotMeasurement

v1.02 (24/04/20)
----------------

- Rename functions :
	- listDevices to TDC_listDevices
	- openDevice to TDC_openDevice
	- closeDevice to TDC_closeDevice
- Modify functions :
	- TDC_setSyncInputConfig
	- TDC_getSyncInputConfig
	- TDC_setChannelConfig
	- TDC_getChannelConfig
- Add functions :
	- TDC_setSyncSource
	- TDC_getSyncSource
	- TDC_setSyncDivider
	- TDC_getSyncDivider
	- TDC_setChannelDelay
	- TDC_getChannelDelay

v1.01 (14/11/19)
----------------

- Add functions :
	- TDC_setResultFormat
	- TDC_getResultFormat
	- TDC_setCrossCorrelationMode
	- TDC_getCrossCorrelationData
	- TDC_getCh1Histogram
	- TDC_getCh2Histogram
	- TDC_getCh3Histogram
	- TDC_getCh4Histogram

v1.00 (29/04/19)
----------------

- First release
