.. _wrapper:

C++ Wrapper
===========

Wrapper Advantage
~~~~~~~~~~~~~~~~~

A C++ wrapper has been created for several reasons.
	- To make  functions easy to use.
	- To allow multiple HTDC device control in the same application and at the same time.
	- To link Dynamic Library inside C++ code and not in the project configuration.

.. note::

	Except OpenDevice function, you do not need to specify iDev when using HTDC wrapper function.

	For example function ``HTDC_getChannelState(short iDev, unsigned char iCh, int *status, unsigned long *nSampleToRecover, unsigned long *nSampleRecovered)`` can be replace by ``ObjectName.GetChannelState(unsigned char iCh, int *status, unsigned long *nSampleToRecover, unsigned long *nSampleRecovered)``

C++ code
~~~~~~~~

Here is an example of how to use this wrapper to recover data from 2 HTDC :

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "HTDC_wrapper.h"
	#include "HTDC.h"

	// Select shared library compatible to current operating system
	#ifdef _WIN32
	#define DLL_PATH L"HTDC.dll"
	#elif __unix
	#define DLL_PATH "libHTDC.so"
	#else
	#define DLL_PATH "libHTDC.dylib"
	#endif

	int main(int argc, const char* argv[]) {
		short iDev = 0;
		short ret;
		char* devicesList[10];
		short numberDevices;
		unsigned short arg1[4], arg2[4];
		unsigned long ul_arg1[4], ul_arg2[4];

		HTDC_wrapper HTDC0(DLL_PATH);
		HTDC_wrapper HTDC1(DLL_PATH);

		/*    ListDevices function    */
		// List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
		if (HTDC0.ListDevices(devicesList, &numberDevices) == 0) {
			if (numberDevices == 0) {
				cout << endl << "    Please connect AT device !" << endl << endl;
				do {
					delay(500);
					HTDC0.ListDevices(devicesList, &numberDevices);
				} while (numberDevices == 0);
			}
		}

		// Open communication with device 0
		printf(" -%u: %s\n", 0, devicesList[0]);
		HTDC0.OpenDevice(0);
		printf("\n HTDC %d-> Communication Open\n\n", 0);

		// Open communication with device 1
		printf(" -%u: %s\n", 1, devicesList[1]);
		HTDC1.OpenDevice(1);
		printf("\n HTDC %d-> Communication Open\n\n", 1);

		// Recover Channel 1 state from both HTDC
		HTDC0.GetChannelState(1, (int*)&arg1[0], &ul_arg1[0], &ul_arg1[1]);
		printf("HTDC 1 -> Ch[1]: \n");
		printf("    + State   = %u \n", arg1[0]);
		printf("    + Consign = %lu \n", ul_arg1[0]);
		printf("    + Monitor = %lu \n", ul_arg1[1]);

		HTDC1.GetChannelState(1, (int*)&arg2[0], &ul_arg2[0], &ul_arg2[1]);
		printf("HTDC 2 -> Ch[1]: \n");
		printf("    + State   = %u \n", arg2[0]);
		printf("    + Consign = %lu \n", ul_arg2[0]);
		printf("    + Monitor = %lu \n", ul_arg2[1]);

		// Wait some time
		delay(2000);

		/*    CloseDevice function    */
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (HTDC0.CloseDevice() == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;
		if (HTDC1.CloseDevice() == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;

		// Call class destructor
		HTDC0.~HTDC_wrapper();
		HTDC1.~HTDC_wrapper();

		return 0;
	}